/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.apispring.controller;

import com.helloworld.apispring.model.dao.PersonaRepository;
import com.helloworld.apispring.model.entity.Persona;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@Component

public class PersonaServicio {
 @Autowired
    
    private PersonaRepository personaRepository;

    public PersonaServicio() {
    }
    
    
    
    
    public long CrearPersona(Persona persona){
        return personaRepository.CrearPersona(persona);
    }
    
}
