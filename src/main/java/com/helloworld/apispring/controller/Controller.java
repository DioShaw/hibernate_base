/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.apispring.controller;




import com.helloworld.apispring.model.entity.Persona;
import com.helloworld.apispring.model.entity.RankingFifa;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/v1")
public class Controller {
   
   @Autowired
 private RankingServicio rankingServicio;

 @RequestMapping(value = "/equipos/", method = RequestMethod.GET)
 public ResponseEntity<List<RankingFifa>> obtenerEquipos() {
 List<RankingFifa> equipos = rankingServicio.getAllRanking();
 return new ResponseEntity<List<RankingFifa>>(equipos, HttpStatus.OK);
 }
 
 
   @Autowired
 private PersonaServicio personaServicio;
   

 @RequestMapping(value = "/personas/", method = RequestMethod.POST,consumes =  MediaType.APPLICATION_JSON_VALUE)
 public ResponseEntity<String> crearPersona(@RequestBody Persona persona) {
 String resultado = "Se creo persona con ID: "+personaServicio.CrearPersona(persona);
 return new ResponseEntity<String>(resultado, HttpStatus.OK);
 }
     

}
