/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.helloworld.apispring.model.dao;

import com.helloworld.apispring.model.entity.Persona;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;



   @Repository
    @Transactional
    

public class PersonaRepository {
    
    @Autowired
    private SessionFactory SessionFactory;

    public SessionFactory getSessionFactory() {
        return SessionFactory;
    }

    public void setSessionFactory(SessionFactory SessionFactory) {
        this.SessionFactory = SessionFactory;
    }

    public PersonaRepository() {
    }
    
    
    public long CrearPersona(Persona persona){
        getSessionFactory().getCurrentSession().save(persona);
        
        
        return persona.getId();
    }
    
    
 
    
}
